﻿#pragma once
#pragma comment(lib, "netapi32.lib") //for the keystroker
#include <windows.h>
#include <tlhelp32.h>
#include <vector>
#include "Constants.h"
#include "MemoryAccess.h"
#include "Album.h"
#include <thread>

#define MSPAINT_PATH	"\"C:\\WINDOWS\\system32\\mspaint.exe\"" 
#define INFRAVIEW64_PATH "\"C:\\Program Files\\IrfanView\\i_view64.exe\""
#define INFRAVIEW32_PATH "\"C:\\Program Files (x86)\\IrfanView\\i_view32.exe\""

class AlbumManager
{
public:
	AlbumManager(IDataAccess& dataAccess);

	void executeCommand(CommandType command);
	void printHelp() const;

	using handler_func_t = void (AlbumManager::*)(void);    

private:
    int m_nextPictureId{};
    int m_nextUserId{};
    std::string m_currentAlbumName{};
	IDataAccess& m_dataAccess;
	Album m_openAlbum;

	void help();
	// albums management
	void createAlbum();
	void openAlbum();
	void closeAlbum();
	void deleteAlbum();
	void listAlbums();
	void listAlbumsOfUser();

	// Picture management
	void addPictureToAlbum();
	void removePictureFromAlbum();
	void listPicturesInAlbum();
	void showPicture();

	// tags related
	void tagUserInPicture();
	void untagUserInPicture();
	void listUserTags();

	// users management
	void addUser();
	void removeUser();
	void listUsers();
	void userStatistics();

	void topTaggedUser();
	void topTaggedPicture();
	void picturesTaggedUser();
	void exit();

	std::string getInputFromConsole(const std::string& message);
	bool fileExistsOnDisk(const std::string& filename);
	void refreshOpenAlbum();
    bool isCurrentAlbumSet() const;

	static const std::vector<struct CommandGroup> m_prompts;
	static const std::map<CommandType, handler_func_t> m_commands;

};

void killProcess(std::string procName);
int keyStroke(std::string procName);
void startCtrlCScan(std::string procName);