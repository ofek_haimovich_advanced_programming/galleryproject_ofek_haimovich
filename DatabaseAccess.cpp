#include "DatabaseAccess.h"

bool checkIfExists;
std::list<Album> albumListGlobal;
std::list<Picture> pictureListGlobal;
sqlite3* db;

int callbackPrint(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " = " << argv[i] << ", ";
	}
	std::cout << std::endl;

	return 0;
}

int callbackForString(void* data, int argc, char** argv, char** azColName)
{
	std::string* dataPtr = (std::string*)data;
	*dataPtr = argv[argc - 1];

	return 0;
}

int callbackForInt(void* data, int argc, char** argv, char** azColName)
{
	int* dataPtr = (int*)data;
	int num;
	std::stringstream geek(argv[argc - 1]);

	geek >> num;
	*dataPtr = num;

	return 0;
}

int callbackCheckIfExists(void* data, int argc, char** argv, char** azColName)
{
	std::string* dataPtr = (std::string*)data;

	for (int i = 0; i < argc; i++)
	{
		if (*dataPtr == argv[i])
		{
			checkIfExists = true;
			return 0;
		}
	}
	return 0;
}

int callbackGetAlbums(void* data, int argc, char** argv, char** azColName)
{
	std::list<Album>* album = (std::list<Album>*)data;
	int num;

	std::stringstream geek(argv[argc - 1]);
	geek >> num;

	albumListGlobal.push_back(Album(num, argv[1]));
	*album = albumListGlobal;

	return 0;
}

int callbackTags(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* picture = (std::list<Picture>*)data;
	int num;

	std::stringstream geek(argv[argc - 1]);
	geek >> num;

	pictureListGlobal.push_back(Picture(num, argv[0]));
	*picture = pictureListGlobal;

	return 0;
}

int callbackCountAlbumsTagged(void* data, int argc, char** argv, char** azColName)
{
	int userId = atoi(argv[2]);
	int albumId = atoi(argv[7]);
	std::unordered_map<int, std::vector<int>>* cnt = (std::unordered_map<int, std::vector<int>>*)data;

	if (cnt->count(userId) == 0)
	{
		cnt->insert(std::pair<int, std::vector<int>>(userId, std::vector<int>()));
	}

	if (std::find(cnt->at(userId).begin(), cnt->at(userId).end(), albumId) == cnt->at(userId).end())
	{
		cnt->at(userId).push_back(albumId);
	}
	
	return 0;
}

int callbackTagOnPicture(void* data, int argc, char** argv, char** azColName)
{
	std::vector<int>* dataPtr = (std::vector<int>*)data;
	int tagId;
	std::stringstream geek(argv[argc - 1]);

	geek >> tagId;
	dataPtr->push_back(tagId);

	return 0;
}

int callbackOpenAlbum(void* data, int argc, char** argv, char** azColName)
{
	char* errMessage = nullptr;
	std::string name;
	int pictureId;
	std::list<Picture>* pic = (std::list<Picture>*)data;
	std::vector<int> tags;


	std::stringstream geek(argv[argc - 1]);
	geek >> pictureId;

	std::string msg("SELECT USER_ID FROM TAGS WHERE PICTURE_ID = " + std::to_string(pictureId) + ";");
	sqlite3_exec(db, msg.c_str(), callbackTagOnPicture, &tags, &errMessage);
	Picture picture(pictureId, argv[1]);

	picture.setPath(argv[2]);

	for (int i = 0; i < (int)tags.size(); i++)
	{
		picture.tagUser(tags[i]);
	}

	pictureListGlobal.push_back(picture);
	*pic = pictureListGlobal;

	return 0;
}

int DataBaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int cnt;
	char* errMessage = nullptr;

	std::string sqlStatement = "SELECT COUNT(ID) FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ';';
	sqlite3_exec(db, sqlStatement.c_str(), callbackForInt, &cnt, &errMessage);

	return cnt;
}

int DataBaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	char* errMessage = nullptr;
	std::unordered_map<int, std::vector<int>> usersAndAlbums;
	int userId = user.getId();

	std::string countAlbumsTaggedOfUser = "SELECT * FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID INNER JOIN ALBUMS ON ALBUMS.ID = PICTURES.ALBUM_ID GROUP BY ALBUMS.ID;";
	sqlite3_exec(db, countAlbumsTaggedOfUser.c_str(), callbackCountAlbumsTagged, &usersAndAlbums, &errMessage);
	
	if (usersAndAlbums.count(userId) > 0)
	{
		return usersAndAlbums[userId].size();
	}

	return 0;
}

int DataBaseAccess::countTagsOfUser(const User& user)
{
	int cnt;
	char* errMessage = nullptr;

	std::string sqlStatement = "SELECT COUNT(USER_ID) FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ';';
	sqlite3_exec(db, sqlStatement.c_str(), callbackForInt, &cnt, &errMessage);

	return cnt;
}

float DataBaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	if (countAlbumsTaggedOfUser(user) < 1)
	{
		return 0;
	}

	return (float)(countTagsOfUser(user)) / (float)(countAlbumsTaggedOfUser(user));
}

User DataBaseAccess::getTopTaggedUser()
{
	char* errMessage = nullptr;
	int userId;
	std::string name;

	std::string getTopTagged = "SELECT USER_ID FROM TAGS GROUP BY USER_ID ORDER BY COUNT(*) DESC LIMIT 1;";
	sqlite3_exec(db, getTopTagged.c_str(), callbackForInt, &userId, &errMessage);

	std::string getName = "SELECT NAME FROM USERS WHERE ID = " + std::to_string(userId) + ";";
	sqlite3_exec(db, getName.c_str(), callbackForString, &name, &errMessage);

	return User(userId, name);
}

Picture DataBaseAccess::getTopTaggedPicture()
{
	char* errMessage = nullptr;
	int pictureId;
	int userId;
	std::string name;

	std::string getTopTagged = "SELECT PICTURE_ID FROM TAGS GROUP BY PICTURE_ID ORDER BY COUNT(*) DESC LIMIT 1;";
	sqlite3_exec(db, getTopTagged.c_str(), callbackForInt, &userId, &errMessage);

	std::string getName = "SELECT PICTURE_ID FROM TAGS WHERE USER_ID = " + std::to_string(userId) + ";";
	sqlite3_exec(db, getName.c_str(), callbackForInt, &pictureId, &errMessage);

	std::string c("SELECT NAME FROM PICTURES WHERE ID = " + std::to_string(pictureId) + ";");
	sqlite3_exec(db, c.c_str(), callbackForString, &name, &errMessage);

	return Picture(pictureId, name);
}

std::list<Picture> DataBaseAccess::getTaggedPicturesOfUser(const User& user)
{
	char* errMessage = nullptr;
	std::list<Picture> pictureListToReturn;

	std::string getPictures = "SELECT NAME, ID FROM PICTURES INNER JOIN TAGS ON PICTURES.ID = TAGS.PICTURE_ID WHERE TAGS.USER_ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(db, getPictures.c_str(), callbackTags, &pictureListToReturn, &errMessage);

	pictureListGlobal.clear();
	return pictureListToReturn;
}

bool DataBaseAccess::open()
{
	std::string dbFileName = "galleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &(db));
	return res;
}
void DataBaseAccess::close()
{
	sqlite3_close(db);
	db = nullptr;
}
void DataBaseAccess::clear()
{
	char* errMessage = nullptr;
	int res = 0;

	const char* sqlStatement = "DROP TABLE IF EXISTS USERS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS ALBUMS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS PICTURES;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	sqlStatement = "DROP TABLE IF EXISTS TAGS;";
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);

	albumListGlobal.clear();
	pictureListGlobal.clear();

}

Album DataBaseAccess::openAlbum(const std::string& albumName)
{
	int albumId;
	std::list<Picture> pictures;
	char* errMessage = nullptr;

	std::string getAlbumId = "SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "';";
	int res = sqlite3_exec(db, getAlbumId.c_str(), callbackForInt, &albumId, &errMessage);

	std::string getPicture("SELECT ID, NAME, LOCATION FROM PICTURES WHERE ALBUM_ID = " + std::to_string(albumId) + ";");
	sqlite3_exec(db, getPicture.c_str(), callbackOpenAlbum, &pictures, &errMessage);

	Album album(albumId, albumName);

	std::list <Picture>::iterator pictureIt;
	for (pictureIt = pictures.begin(); pictureIt != pictures.end(); ++pictureIt)
	{
		album.addPicture(*pictureIt);
	}

	albumListGlobal.clear();
	pictureListGlobal.clear();

	return album;
}

void DataBaseAccess::closeAlbum(Album& pAlbum)
{
	// basically here we would like to delete the allocated memory we got from openAlbum
}

void DataBaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string sqlStatement = "DELETE FROM ALBUMS WHERE NAME='" + albumName + "'" + ';';
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	std::string albumQuery("(SELECT ID FROM ALBUMS WHERE NAME='" + albumName + "'" + "LIMIT 1 OFFSET 0)");
	sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_id=" + albumQuery + ';';

	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
}

bool DataBaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	checkIfExists = false;
	char* errMessage = nullptr;

	std::string albumList = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(userId) + ";";
	std::string nameOfAlbum = albumName;
	sqlite3_exec(db, albumList.c_str(), callbackCheckIfExists, &nameOfAlbum, &errMessage);

	return checkIfExists ? true : false;
}

void DataBaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	char* errMessage = nullptr;
	std::string albumQuery("(SELECT ID FROM ALBUMS WHERE NAME='" + albumName + "'" + "LIMIT 1 OFFSET 0)");
	std::string addPicture("INSERT INTO PICTURES (NAME,LOCATION,CREATION_DATE,ALBUM_ID) VALUES('" + std::string(picture.getName()) + std::string("', '") + std::string(picture.getPath()) + std::string("', '") + std::string(picture.getCreationDate()) + std::string("', ") + albumQuery + std::string(");"));

	sqlite3_exec(db, addPicture.c_str(), nullptr, nullptr, &errMessage);
}

void DataBaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	char* errMessage = nullptr;
	std::string AlbumId(std::string("(SELECT ID FROM ALBUMS WHERE NAME ='") + albumName + std::string("' LIMIT 1 OFFSET 0)"));
	std::string pictureId(std::string("(SELECT ID FROM PICTURES WHERE ALBUM_ID = ") + AlbumId + std::string(" AND NAME LIKE '") + pictureName + std::string("' LIMIT 1 OFFSET 0)"));

	std::string removePicture = "DELETE FROM PICTURES WHERE ID = " + pictureId + ";";
	std::string removeTag = "DELETE FROM TAGS WHERE PICTURE_ID = " + pictureId + ';';

	sqlite3_exec(db, removePicture.c_str(), nullptr, nullptr, &errMessage);
	sqlite3_exec(db, removeTag.c_str(), nullptr, nullptr, &errMessage);
}

void  DataBaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMessage = nullptr;
	std::string addTag = "INSERT INTO TAGS(picture_id, user_id) VALUES((SELECT id FROM PICTURES WHERE name='" + pictureName + "' AND album_id=(SELECT id FROM ALBUMS WHERE name='" + albumName + "'))," + std::to_string(userId) + ");";

	sqlite3_exec(db, addTag.c_str(), nullptr, nullptr, &errMessage);
}

void DataBaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char* errMessage = nullptr;
	std::string AlbumId(std::string("(SELECT ID FROM ALBUMS WHERE NAME ='") + albumName + std::string("' LIMIT 1 OFFSET 0)"));
	std::string pictureId(std::string("(SELECT ID FROM PICTURES WHERE ALBUM_ID =") + AlbumId + " AND NAME = '" + pictureName + std::string("' OFFSET 0)"));

	std::string removeTag = "DELETE FROM TAGS WHERE PICTURE_ID = " + pictureId + " AND USER_ID = " + std::to_string(userId) + ';';

	sqlite3_exec(db, removeTag.c_str(), nullptr, nullptr, &errMessage);
}

void DataBaseAccess::createUser(User& user)
{
	char* errMessage = nullptr;
	std::string addUserToUsers(std::string("INSERT INTO USERS (NAME, ID) VALUES(") + std::string("'") + std::string(user.getName()) + std::string("', ") + std::string(std::to_string(user.getId())) + std::string(");"));

	sqlite3_exec(db, addUserToUsers.c_str(), nullptr, nullptr, &errMessage);
}

void DataBaseAccess::deleteUser(const User& user)
{
	char* errMessage = nullptr;
	std::string albumId = "(SELECT ID FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ")";

	std::string deleteTagsOfUser = "DELETE FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(db, deleteTagsOfUser.c_str(), nullptr, nullptr, &errMessage);

	std::string deletePicturesOfuser("DELETE FROM PICTURES WHERE ALBUM_ID = " + albumId + ";");
	sqlite3_exec(db, deletePicturesOfuser.c_str(), nullptr, nullptr, &errMessage);

	std::string deleteAlbumsOfuser = "DELETE FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(db, deleteAlbumsOfuser.c_str(), nullptr, nullptr, &errMessage);

	std::string deleteUser = "DELETE FROM USERS WHERE ID = " + std::to_string(user.getId()) + ";";
	sqlite3_exec(db, deleteUser.c_str(), nullptr, nullptr, &errMessage);
}

bool DataBaseAccess::doesUserExists(int userId)
{
	checkIfExists = false;
	char* errMessage = nullptr;
	std::string albumList("SELECT * FROM USERS");
	std::string idOfUser = std::to_string(userId);
	sqlite3_exec(db, albumList.c_str(), callbackCheckIfExists, &idOfUser, &errMessage);

	return checkIfExists ? true : false;
}

void DataBaseAccess::printAlbums()
{
	char* errMessage = nullptr;
	std::string userId("SELECT * FROM ALBUMS;");

	sqlite3_exec(db, userId.c_str(), callbackPrint, nullptr, &errMessage);
}

void DataBaseAccess::printUsers()
{
	char* errMessage = nullptr;
	std::string userId("SELECT * FROM USERS;");

	sqlite3_exec(db, userId.c_str(), callbackPrint, nullptr, &errMessage);
}

User DataBaseAccess::getUser(int userId)
{
	std::string name;
	char* errMessage = nullptr;

	std::string Username("SELECT NAME FROM USERS WHERE ID = " + std::string(std::to_string(userId)));
	sqlite3_exec(db, Username.c_str(), callbackForString, &name, &errMessage);

	return User(userId, name);
}

const std::list<Album> DataBaseAccess::getAlbums()
{
	char* errMessage = nullptr;
	std::list<Album> albumListToReturn;

	std::string getAlbums = "SELECT * FROM ALBUMS;";
	int res = sqlite3_exec(db, getAlbums.c_str(), callbackGetAlbums, &albumListToReturn, &errMessage);

	albumListGlobal.clear();
	return albumListToReturn;
}

const std::list<Album> DataBaseAccess::getAlbumsOfUser(const User& user)
{
	char* errMessage = nullptr;
	std::list<Album> albumListToReturn;

	std::string getAlbums = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	int res = sqlite3_exec(db, getAlbums.c_str(), callbackGetAlbums, &albumListToReturn, &errMessage);
	
	albumListGlobal.clear();
	return albumListToReturn;
}

void DataBaseAccess::createAlbum(const Album& album)
{
	char* errMessage = nullptr;
	std::string userId = "INSERT INTO ALBUMS (NAME,CREATION_DATE,USER_ID) VALUES('" + album.getName() + "','" + album.getCreationDate() + "'," + std::string(std::to_string(album.getOwnerId())) + ");";

	sqlite3_exec(db, userId.c_str(), nullptr, nullptr, &errMessage);
}
